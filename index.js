let trainerA = {
	name: "Ash ketchum",
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log(trainerA.pokemon[0] + " I choose you!");
	}
}

console.log(trainerA);

console.log('Result of dot notation:');
console.log(trainerA.name);
console.log('Result of Square bracket notation:')
console.log(trainerA['pokemon']);
console.log('Result of Square talk method:');
trainerA.talk();

// function objects

function Pokemon(name, level, health, attack){
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//methods
	this.tackle = function(target){
		let targetHealth = target.health - this.attack
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));

		if(targetHealth <= 0){
			target.faint()
		}
	}

	this.faint = function(){
		console.log(this.name + " fainted.")

	}
}

let pikachu = new Pokemon("Pikachu", 12, 24, 12);
console.log(pikachu)
let geodude = new Pokemon("Geodude", 8, 16, 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100);
console.log(mewtwo);


geodude.tackle(pikachu);
mewtwo.tackle(geodude);


